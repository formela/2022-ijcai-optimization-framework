from graph_utils import Graph
from logs import Logger, TrainLogger
from model import objectives_dict, Strategy, Game
from trainer import StrategyTrainer
from utils import set_seed, parse_params


@parse_params
def main(seed, verbosity, results_dir, epochs, graph_params, strategy_params, objective, objective_params,
         trainer_params, logger_params=None):
    logger_params = logger_params or {}

    graph = Graph(**graph_params, results_dir=results_dir, verbose=(verbosity >= 2))
    logger = Logger(results_dir, **logger_params, verbose=(verbosity >= 1))

    for epoch in range(epochs):
        set_seed(seed, epoch)

        strategy = Strategy(graph.mask, **strategy_params)
        objective_instance = objectives_dict[objective](graph, **objective_params)

        game = Game(strategy=strategy, objective=objective_instance)
        train_logger = TrainLogger(epoch, tensorboard=False, log_dir=results_dir)

        trainer = StrategyTrainer(game, train_logger, **trainer_params, verbose=(verbosity >= 3))
        metrics = trainer.train()
        logger.update(*metrics)

    logger.dump()


if __name__ == '__main__':
    main()
