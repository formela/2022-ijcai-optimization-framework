# General Optimization Framework for Recurrent Reachability Objectives (IJCAI 2022)

Implementation of an efficient strategy synthesis algorithm for robot motion planning problem with respect to recurrent reachability objectives. The objectives are parameterized by the expected time needed to visit one position from another, the variance of this time, and also the frequency of visits to each position.

To appear in [IJCAI 2022](https://ijcai-22.org/).

David Klaska, Antonin Kucera, Vit Musil, Vojtech Rehak. General Optimization Framework for Recurrent Reachability Objectives. In Proceedings of the Thirty-First International Joint Conference on Artificial Intelligence Main Track. Pages 4642-4648. https://doi.org/10.24963/ijcai.2022/644

The arXive version is available [here](https://arxiv.org/abs/2205.14057).

Up-to-date version of Regstar is at [gitlab.fi.muni.cz/formela/regstar](https://gitlab.fi.muni.cz/formela/regstar/).


# Strategy Optimization

## Installation

Create virtual environment (`pipenv`) and install all packages:

1. Make sure you have `python 3.9` and `pipenv` installed
1. Install packages from pipfile (at your own risk with `--skip-lock`)
   > python3 -m pipenv install
1. Enter the virtualenv 
   > python3 -m pipenv shell

## Execution

Enter the virtual environment:
> python3 -m pipenv shell

Run the optimization:
> python3 train.py path/to/your/settings.yml

where `path/to/your/settings.yml` is the path to your settings file.


## Experiments setting

Experiment parameters are stored in `YAML` setting files.
They contain:

* `graph_params` graph specification
* `results_dir: results/experiment_name/{timestamp}` path to store the results. The use of `{timestamp}` is available.
* `epochs: <int>` number of optimization trials starting from random initialization
* `seed: null` set seed for reproducibility
* `verbosity: 0-3` 0=silent, 1=prints val per epoch, 2=prints val per step, 3=prints all (debug mode)
* `trainer_params` optimization parameters
* `objective_params` objective parameters
* `strategy_params` strategy parameters
* `logger_params` logging options


### Available settings

Directory `settings/` contains files needed to perform the experiments from the paper.


## Results

The results, visualizations and logs are stored in `results_dir`. They include:

* `settings.yml` A copy of the configuration used
* `stats_epoch.csv` Detailed statistics for all epochs
* `stats_final.csv` Statistics summary over all epochs
* `graph.pdf` Basic graph visualization

*   If `logger_params.plot=True`
    * `training_progress.pdf` Plots of strategy values during optimization
    * `avg_times.pdf` Plot of average runtimes
    * `epoch_statistics.pdf` Epoch statistics visualizations
  
*   If `logger_params.save_logs=True`
    * `stats_full.csv` Detailed statistics for all steps of all epochs
 
*   If `logger_params.save_checkpoints=True`
    * `checkpoints/strategy_sd<int>.pth` State dict of the best strategy found

* `tensorboard/epoch_<int>` Tensorboardx log files.

To monitor the ascent progress during optimization, run in the virtual environment

> tensorboard --logdir results_dir

