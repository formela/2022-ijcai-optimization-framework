import numpy
import pandas
import os
import matplotlib.pyplot as plt
import torch
from time import process_time
from tensorboardX import SummaryWriter
from postprocess.plotting import (
    plot_statistics,
    plot_values,
    plot_times
)


class Logger(object):
    def __init__(self, log_dir, plot=True, save_logs=False, save_checkpoints=False, save_strategies=False,
                 mode='min', verbose=False):
        self.mode = check_mode(mode)
        self.log_dir = log_dir
        self.verbose = verbose
        self.plot = plot
        self.save_logs = save_logs
        self.save_checkpoints = save_checkpoints
        self.save_strategies = save_strategies
        plt.rcParams.update({
            "text.usetex": False
        })
        epoch_columns = ['epoch', 'steps', 'end_val', 'best_val', 'best_val_at', 'avg_val', 'epoch_time',
                         'avg_step_time', 'avg_forward_time', 'avg_backward_time', 'avg_optimizer_time',
                         'avg_eval_time']
        self.epoch_data = pandas.DataFrame(columns=epoch_columns)
        self.log_columns = ['epoch', 'step', 'val', 'val_train', 'loss', 'step_time', 'forward_time', 'backward_time',
                            'optimizer_time', 'eval_time']
        self.extras_columns = set()
        self.log_data = pandas.DataFrame(columns=self.log_columns)
        self.strategies = []
        self.start_time = process_time()

    def update(self, metrics, times, strategy, log_data):
        self.log_data = self.log_data.append(log_data, ignore_index=True)
        extras = metrics.pop('extras', {})
        self.extras_columns.update(extras.keys())
        epoch_data = dict(**metrics, **times, **extras)

        if strategy:
            self.strategies.append((metrics['epoch'], *strategy))

        if self.verbose:
            self.print_epoch(epoch_data)

        epoch_df = pandas.DataFrame(epoch_data, index=[metrics['epoch']])
        self.epoch_data = self.epoch_data.append(epoch_df)

    def dump(self):
        total_time = process_time() - self.start_time
        for key in ['epoch', 'steps', 'best_val_at']:
            self.epoch_data[key] = self.epoch_data[key].astype('int')
        self.epoch_data.sort_index(inplace=True)
        self.epoch_data.to_csv(os.path.join(self.log_dir, 'stats_epoch.csv'), index=False)
        if self.save_logs:
            self.log_data.to_csv(os.path.join(self.log_dir, 'stats_full.csv'), index=False)

        best_val_at_epoch = get_best_at(self.epoch_data['best_val'], self.mode)
        final_data = dict(
            epochs=max(self.epoch_data['epoch']),
            best_val=get_best(self.epoch_data['best_val'], self.mode),
            best_val_at_epoch=best_val_at_epoch,
            avg_best_val_at=self.epoch_data['best_val_at'].mean(),
            avg_val=self.epoch_data['avg_val'].mean(),
            avg_epoch_time=self.epoch_data['epoch_time'].mean(),
            avg_step_time=self.epoch_data['avg_step_time'].mean(),
            total_time=total_time
        )
        final_data.update({key: self.epoch_data[key][best_val_at_epoch] for key in self.extras_columns})
        final_data_series = pandas.Series(final_data)
        final_data_series.to_csv(os.path.join(self.log_dir, 'stats_final.csv'), header=False)

        if self.verbose:
            self.print_stats(final_data)

        if self.save_checkpoints:
            checkpoints = os.path.join(self.log_dir, 'checkpoints')
            os.makedirs(checkpoints, exist_ok=True)
            for (epoch, _, state_dict) in self.strategies:
                torch.save(state_dict, os.path.join(checkpoints, f'strategy_sd_{epoch:02d}.pth'))

        if self.save_strategies:
            strategies = os.path.join(self.log_dir, 'strategies')
            os.makedirs(strategies, exist_ok=True)
            for (epoch, strategy, _) in self.strategies:
                numpy.save(os.path.join(strategies, f'strategy_{epoch:02d}.npy'), strategy, allow_pickle=True)

        if self.plot:
            if final_data['epochs'] > 1:
                plot_statistics(self.epoch_data, os.path.join(self.log_dir, 'epoch_statistics.pdf'))
            plots = dict(val='value', val_train='training value', loss='loss')
            extras = {k: k for k in self.log_data.columns if k not in self.log_columns}
            plot_values(self.log_data, os.path.join(self.log_dir, 'training_progress.pdf'), plots={**plots, **extras})
            plot_times(self.log_data, os.path.join(self.log_dir, 'avg_times.pdf'))

        return {**final_data, 'df_epoch': self.epoch_data}

    def print_epoch(self, epoch_data):
        str_fmt = 'epoch {epoch}:'
        str_fmt += '\tend val={end_val:.1f}'
        str_fmt += '\tbest val={best_val:.1f} at {best_val_at}'
        for key in self.extras_columns:
            str_fmt += f'\t{key}={{{key}:.1f}}'
        str_fmt += '\ttime={epoch_time:.2f}'
        str_fmt += '\tavg step time={avg_step_time:.3f}'
        print(str_fmt.format(**epoch_data))

    def print_stats(self, final_data):
        str_fmt = '-------------'
        str_fmt += '\nbest val\t= {best_val:.1f} at epoch = {best_val_at_epoch}'
        for key in self.extras_columns:
            str_fmt += f'\n{key}\t={{{key}:.1f}}'
        str_fmt += '\navg epoch time\t= {avg_epoch_time:.2f}'
        str_fmt += '\navg step time\t= {avg_step_time:.3f}'
        str_fmt += '\ntotal time\t= {total_time:.1f}'
        print(str_fmt.format(**final_data))


class TrainLogger(object):
    def __init__(self, epoch, tensorboard=False, log_dir=None, mode='min'):
        self.mode = check_mode(mode)
        self.epoch = epoch + 1
        self.steps = None
        self.val = []
        self.val_train = []
        self.loss = []
        self.forward_time = []
        self.backward_time = []
        self.optimizer_time = []
        self.extras = {}
        self.eval_time = []
        self.step_time = []
        self.tb_logger = None
        self.best_val = - self.mode * float('inf')
        self.strategy = None
        self.state_dict = None
        self.tensorboard = tensorboard
        if tensorboard:
            self.tb_logger = SummaryWriter(log_dir=os.path.join(log_dir, f'tensorboard/epoch_{epoch:02d}'))
        self.start_time = process_time()

    def update(self, step_num, val, val_train, loss,
               strategy, state_dict,
               f_time, b_time, o_time, e_time,
               extras=None):
        extras = extras or {}

        if self.tb_logger is not None:
            self.tb_logger.add_scalar(f'val', val, step_num)
            self.tb_logger.add_scalar(f'val_train', val_train, step_num)
            self.tb_logger.add_scalar(f'loss', loss, step_num)
            for key, v in extras.items():
                self.tb_logger.add_scalar(key, v, step_num)
        if self.mode * val > self.mode * self.best_val:
            self.best_val = val
            self.strategy = strategy
            self.state_dict = state_dict
        self.val.append(val)
        self.val_train.append(val_train)
        self.loss.append(loss)
        self.forward_time.append(f_time)
        self.backward_time.append(b_time)
        self.optimizer_time.append(o_time)
        self.eval_time.append(e_time)
        for key, v in extras.items():
            self.extras.setdefault(key, []).append(v)

    def dump(self):
        epoch_time = process_time() - self.start_time
        if self.tensorboard:
            self.tb_logger.close()
        data = dict(
                epoch=self.epoch,
                val=self.val,
                val_train=self.val_train,
                loss=self.loss,
                forward_time=self.forward_time,
                backward_time=self.backward_time,
                optimizer_time=self.optimizer_time,
                eval_time=self.eval_time
            )
        data.update(self.extras)
        df = pandas.DataFrame(data)

        steps = len(self.val)
        df['step'] = range(1, steps + 1)
        df['step_time'] = df['forward_time'] + df['backward_time'] + df['optimizer_time'] + df['eval_time']

        best_val_at = get_best_at(df['val'], self.mode)
        metrics = dict(
            epoch=self.epoch,
            steps=steps,
            end_val=df['val'].iloc[-1],
            best_val=get_best(df['val'], self.mode),
            # avg_val=(df['val'].sum() + (self.steps - steps) * df['val'].iloc[-1]) / self.steps,
            avg_val=df['val'].iloc[20:].mean(),
            best_val_at=best_val_at,
            extras={key: val[best_val_at - 1] for key, val in self.extras.items()}
        )
        times = dict(
            epoch_time=epoch_time,
            avg_step_time=df['step_time'].mean(),
            avg_forward_time=df['forward_time'].mean(),
            avg_backward_time=df['backward_time'].mean(),
            avg_optimizer_time=df['optimizer_time'].mean(),
            avg_eval_time=df['eval_time'].mean(),
        )
        strategy = (
            self.strategy,
            self.state_dict
        )
        return metrics, times, strategy, df


def check_mode(mode):
    if mode == 'max':
        return 1
    elif mode == 'min':
        return -1
    else:
        raise AttributeError(f'Unknown TrainLogger mode: {mode}')


def get_best(df, mode):
    return df.max() if mode == 1 else df.min()


def get_best_at(df, mode):
    return (df.argmax() if mode == 1 else df.argmin()) + 1
