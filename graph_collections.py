import networkx
from copy import deepcopy


"""
    Add your favourite graph here!
    -> don't forget to add it to 'graphs_dict' to make it available in settings
"""


def hardcoded(nodes, targets, edges, settings=None):
    settings = (settings or {}).copy()
    node_att = settings.pop('node_att', {})
    node_att.update(target=False)
    target_att = settings.pop('target_att', {})
    target_att.update(target=True)
    edge_att = settings.pop('edge_att', {})

    nodes = [(node, {**node_att, **(att or {})}) for node, att in nodes.items()]
    targets = [(target, {**target_att, **(att or {})}) for target, att in targets.items()]
    edges = [(*edge.pop('nodes'), {**edge_att, **edge}) for edge in deepcopy(edges)]

    directed = settings.pop('directed', True)
    graph = networkx.DiGraph() if directed else networkx.Graph()
    graph.add_nodes_from(targets + nodes)
    graph.add_edges_from(edges)
    graph.name = 'hardcoded'

    return graph


graphs_dict = {
    'hardcoded': hardcoded
}
