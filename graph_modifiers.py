from collections import defaultdict


"""
    Add any graph modification!
    -> don't forget to add it to 'graph_modifier_from_string' to make it available in settings
"""

def floyd_warshall(graph, visiting_bonus, verbose=False):
    dist = defaultdict(lambda: defaultdict(lambda: float("inf")))
    for s, t, length in graph.edges.data('len'):
        dist[s][t] = min(length - visiting_bonus, dist[s][t])

    for w in graph:
        dist_w = dist[w]
        for u in graph:
            dist_u = dist[u]
            for v in graph:
                d = dist_u[w] + dist_w[v]
                if d < dist_u[v]:
                    dist_u[v] = d

    edges_to_remove = []
    for s, t, length in graph.edges.data('len'):
        if dist[s][t] < length - visiting_bonus:
            edges_to_remove.append((s, t))
            if verbose:
                print(f'  Removing edge: {s}--{t}')
    graph.remove_edges_from(edges_to_remove)
    return graph


graph_modifiers_dict = {
    'floyd_warshall': floyd_warshall
}
