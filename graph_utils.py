import os
from collections import Counter
import torch
import networkx
import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from termcolor import cprint
from graph_collections import graphs_dict
from graph_modifiers import graph_modifiers_dict


class Graph(object):
    def __init__(self, loader, loader_params, results_dir=None, modifiers=None, verbose=False, name=None):
        self.verbose = verbose
        self.results_dir = results_dir
        self.name = name
        self.graph = load_graph_with_modifiers(loader, loader_params, modifiers, verbose)

        self.graph = self.graph.to_directed()
        self.targets, self.non_targets = [], []
        for (node, is_target) in self.graph.nodes.data('target'):
            (self.non_targets, self.targets)[is_target].append(node)

        self.targets_n = len(self.targets)
        self.costs = [self.graph.nodes[target]['cost'] for target in self.targets]
        self.node_list = self.targets + self.non_targets
        self.node_map = {node: index for index, node in enumerate(self.node_list)}

        self.aug_graph = make_graph_augmented(self.graph)
        self.aug_node_list = list(self.aug_graph.nodes())
        self.aug_nodes_n = self.aug_graph.number_of_nodes()
        self.aug_node_map = {node: index for index, node in enumerate(self.aug_node_list)}

        times = networkx.to_numpy_array(self.aug_graph, nodelist=self.aug_node_list, weight='len')
        self.times = torch.from_numpy(times)
        self.mask = self.times > 0
        self.longest = self.times.max(dim=0).values
        self.costs = torch.tensor(self.costs)

        self.target_mask = torch.ones((self.targets_n, self.aug_nodes_n), dtype=torch.bool)
        for i, target in enumerate(self.targets):
            mem = self.graph.nodes[target]['memory']
            loc = self.aug_node_map[(target, 0)]
            self.target_mask[i, loc:loc + mem] = 0

        if verbose:
            self.print_graph_stats()
        if results_dir:
            self.draw_graph()

    def __repr__(self):
        return self.name or self.graph.name or self.__class__.__name__

    def print_strategy(self, strategy, style='label', precision=3):
        cprint('Strategy:', 'blue')
        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(' ', np.array2string(strategy, prefix='  '))
        else:
            for (source, ms), (target, mt) in self.aug_graph.edges():
                source_index = self.aug_node_map[(source, ms)]
                target_index = self.aug_node_map[(target, mt)]
                prob = strategy[source_index, target_index]
                if prob > 0:
                    if style == 'label':
                        print(f'  {source} ({ms})--{target} ({mt}):\t{prob:.{precision}f}')
                    elif style == 'index':
                        print(f'  {source_index}--{target_index}:\t{prob:.{precision}f}')
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')

    def print_graph_stats(self):
        cprint(f'Nodes: ({self.graph.number_of_nodes()})', 'blue')
        for node in self.graph.nodes.data():
            print('  ', node)
        cprint(f'Edges: ({self.graph.number_of_edges()})', 'blue')
        for edge in self.graph.edges.data():
            print('  ', edge)
        cprint(f'Incidence matrix', 'blue')
        with np.printoptions(precision=1, suppress=True, threshold=np.inf, linewidth=np.inf):
            print(np.array2string(networkx.to_numpy_matrix(self.graph, nodelist=self.node_list, weight='len')))
        lengths = [l for _, _, l in self.graph.edges.data('len')]
        cprint('Graph statistics:', 'blue')
        print(f'  edge length: min={min(lengths)}\tmax={max(lengths)}\tavg={np.mean(lengths):.2f}')
        print(f'  degrees:')
        if self.graph.is_directed():
            out_deg = Counter(d for n, d in self.graph.out_degree())
            in_deg = Counter(d for n, d in self.graph.in_degree())
            for (out_degree, out_count), (in_degree, in_count) in zip(out_deg.most_common(), in_deg.most_common()):
                print(f'   in={in_degree:01d}:\t{in_count}x\tout={out_degree:01d}:\t{out_count}x')

        torch.set_printoptions(threshold=float('inf'), linewidth=200)
        cprint('Mask:', 'blue')
        print(self.mask)
        cprint('Times:', 'blue')
        print(self.times)
        cprint('Target mask:', 'blue')
        print(self.target_mask)
        cprint('longest:', 'blue')
        print(self.longest)
        cprint('costs:', 'blue')
        print(self.costs)

    def draw_graph(self):
        pos = networkx.spring_layout(self.graph)
        networkx.draw(self.graph, with_labels=True, pos=pos)
        labels = {tuple(edge): label for *edge, label in self.graph.edges.data('len')}
        networkx.draw_networkx_edge_labels(self.graph, pos=pos, edge_labels=labels, label_pos=0.3)
        plt.savefig(os.path.join(self.results_dir, 'graph.pdf'))


def graph_loader_from_string(loader):
    return graphs_dict[loader]


def graph_modifier_from_string(modifier):
    return graph_modifiers_dict[modifier]


def load_graph_with_modifiers(loader, loader_params, modifiers, verbose=False):
    graph = graph_loader_from_string(loader)(**loader_params)
    modifiers = modifiers or {}
    if verbose:
        cprint('Graph preprocessing:', 'green')
    for modifier, modifier_params in modifiers.items():
        if verbose:
            cprint(f' {modifier}', 'green')
        graph = graph_modifier_from_string(modifier)(graph, **modifier_params, verbose=verbose)
    if verbose:
        print('done')
    return graph


def make_graph_augmented(graph):
    aug_graph = networkx.DiGraph()

    for node, att in graph.nodes.data():
        aug_nodes = [((node, m), att) for m in range(att['memory'])]
        aug_graph.add_nodes_from(aug_nodes)

    for source, target, att in graph.edges.data():
        source_memory = graph.nodes[source]['memory']
        target_memory = graph.nodes[target]['memory']
        mem_product = product(range(source_memory), range(target_memory))
        aug_edges = [((source, ms), (target, mt), att) for ms, mt in mem_product]
        aug_graph.add_edges_from(aug_edges)

    return aug_graph

